#ifndef _NODE_H_
#define _NODE_H_

struct Node
{
    int value;
    bool bloom;
    Node* left;
    Node* right;
    Node* parent;
};

#endif