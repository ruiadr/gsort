#include <cstddef>
#include "Gardener.h"

Gardener::Gardener(Php::Value &array):
    array(array),
    index(0)
{
    int size = this->array.size();
    if (size > 0) {
        this->tree = new Tree(array[0]);
        this->smallest = this->tree->root;
        
        if (size > 1) {
            // On démarre à 1 puisque l'index 0 a déjà été
            // pris en charge.
            for (int i = 1; i < size; ++i) {
                this->addValue(this->array[i]);
            }
            
            this->orderTreeRecursive(this->smallest);
        }
    }
}

Gardener::~Gardener()
{
    delete this->smallest;
    delete this->tree;
}

// private:

void Gardener::addValue(int value)
{
    Node* node = this->tree->addValue(value);
    if (node->value < this->smallest->value) {
        // Référence vers le noeud contenant la valeur la plus petite.
        this->smallest = node;
    }
}

void Gardener::appendNode(Node* node)
{
    if (node->bloom == false) {
        node->bloom = true; // Flag d'utilisation.
        this->array[this->index++] = node->value;
    }
}

void Gardener::orderTreeRecursive(Node* node)
{
    // L'ordre des tests est important.
    // Avec un tri croissant, il faut descendre vers la gauche
    // pour obtenir la valeur la plus faible.
    if (node->left != NULL) {
        // On descend vers la gauche, donc vers une valeur
        // inférieure à celle de node.
        this->orderTreeRecursive(node->left);
    } else if (node->right != NULL) {
        // En descendant vers la droite, on se dirige
        // vers une valeur supérieure à celle de node, donc ici
        // la valeur de node peut être considérée comme la prochaine
        // valeur de la liste.
        this->appendNode(node);
        this->orderTreeRecursive(node->right);
    } else if (node->parent != NULL) {
        // Aucune descente (gauche ou droite), la valeur de node
        // peut être considérée comme la prochaine valeur
        // de la liste.
        this->appendNode(node);
        
        // Le node courant vient d'être pris en compte,
        // on le retire du parent en fonction de s'il est gauche
        // ou droite.
        if (node->parent->right == node) {
            node->parent->right = NULL;
        } else {
            node->parent->left = NULL;
        }
        
        this->orderTreeRecursive(node->parent);
    }
    
    // Si la valeur la plus élevée se trouve être le noeud racine,
    // elle n'a pas été ajoutée à la liste, on la prend en compte ici.
    if (this->tree->root->bloom == false) {
        this->appendNode(this->tree->root);
    }
}