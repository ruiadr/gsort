#include <phpcpp.h>
#include "Gardener.h"

void gsort(Php::Parameters &params)
{
    Php::Value phpArray = params[0];
    if (phpArray.size() > 0) {
        new Gardener(phpArray);
        params[0] = phpArray; // Plante si le tableau est vide.
    }
}

extern "C" {
    PHPCPP_EXPORT void *get_module() 
    {
        static Php::Extension extension("gsort", "1.0");
        
        extension.add<gsort>("gsort", {
            Php::ByRef("array", Php::Type::Array)
        });
        
        return extension.module();
    }
}