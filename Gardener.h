#ifndef _GARDENER_H_
#define _GARDENER_H_

#include <phpcpp.h>
#include "Tree.h"

class Gardener {
    private:
        Php::Value &array;
        int index;
        
        void addValue(int value);
        void appendNode(Node* node);
        void orderTreeRecursive(Node* node);
        
    public:
        Tree* tree;
        Node* smallest;
        
        Gardener(Php::Value &array);
        virtual ~Gardener();
};

#endif