#include <cstddef>
#include "Tree.h"

Tree::Tree(int value):
    root(createNode(value))
{
}

Tree::~Tree()
{
    delete this->root;
}

// public:

Node* Tree::addValue(int value)
{
    Node* node = createNode(value);
    this->addNodeRecursive(this->root, node);
    return node;
}

// private:

Node* Tree::createNode(int value)
{
    Node* node = new Node;
    
    node->value = value;
    node->bloom = false;
    node->left = NULL;
    node->right = NULL;
    node->parent = NULL;
    
    return node;
}

void Tree::addNodeRecursive(Node* root, Node* node)
{
    if (node->value < root->value) {
        if (root->left == NULL) {
            root->left = node;
            node->parent = root;
        } else {
            this->addNodeRecursive(root->left, node);
        }
    } else {
        if (root->right == NULL) {
            root->right = node;
            node->parent = root;
        } else {
            this->addNodeRecursive(root->right, node);
        }
    }
}