<?php

function test($call, $maxTest, $maxArrayValue)
{
    $times = [];
    
    for ($i = 0; $i < $maxTest; ++$i) {
        $t = [];
        
        for ($k = 0; $k < $maxArrayValue; ++$k) {
            do {
                $v = random_int(1, 1000000);
            } while (in_array($v, $t));
            
            $t[] = $v;
        }
        
        shuffle ($t);
        
        $startTime = microtime(true);
        $call($t);
        $endTime = microtime(true);
        
        if ($maxTest === 1 && $maxArrayValue < 11) {
            print_r($t);
        }
        
        $times[] = $endTime - $startTime;
    }
    
    $sec = ((float)array_sum($times) / (float)count($times));
    
    return "$call: $sec";
}

$maxTest = 1;
$maxArrayValue = 10;

print_r (test('sort', $maxTest, $maxArrayValue));
echo "\n";

print_r (test('gsort', $maxTest, $maxArrayValue));
echo "\n";

exit(0);